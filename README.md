## Zadanie 5. ##
### Języki i narzędzia programowania I ###
### Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego ###

=== Zadanie Maksima ===

W tym zadaniu należy zaimplementować wzorzec klasy

template<typename A, typename V> class FunctionMaxima { ... };

służącej do operacji na funkcji, której dziedziną jest pewien zbiór obiektów
typu A. Funkcja przyporządkowuje każdemu elementowi dziedziny wartość typu V.
Od typu A (odpowiednio V) wymagamy, aby możliwe było porównanie x < y,
gdzie x i y są typu A (odpowiednio V).

Powiemy, że x jest lokalnym maksimum funkcji f, gdy spełnione są dwa warunki:
1) x jest najmniejszym (względem <) elementem dziedziny funkcji f lub
   f(x) nie jest mniejsze niż f(l), gdzie l jest największym elementem
   elementem dziedziny f, takim że l < x.
2) x jest największym (względem <) elementem dziedziny funkcji f lub
   f(x) nie jest mniejsze niż f(r), gdzie r jest najmniejszym elementem
   dziedziny f, takim że x < r.

Dodatkową funkcjonalnością klasy FunctionMaxima<A, V> ma być szybki
dostęp do lokalnych maksimów reprezentowanej aktualnie funkcji.

Konkretnie, oczekujemy następujących składowych klasy. Niech n oznacza
aktualną wielkość dziedziny funkcji.

* Powinny być dostępne: kontruktor bezparametrowy (funkcja o pustej
  dziedzinie), konstruktor kopiujący i operator=. Dwa ostatnie powinny mieć
  sensowne działanie.

* // Zwraca wartość w punkcie a, rzuca wyjątek InvalidArg, jeśli a nie
  // należy do dziedziny funkcji. Złożoność najwyżej O(log n).
  V const& value_at(A const& a) const;

* // Zmienia funkcję tak, żeby zachodziło f(a) = v. Jeśli a nie należy do
  // obecnej dziedziny funkcji, jest do niej dodawany. Najwyżej O(log n).
  void set_value(A const& a, V const& v);

* // Usuwa a z dziedziny funkcji. Jeśli a nie należało do dziedziny funkcji,
  // nie dzieje się nic. Złożoność najwyżej O(log n).
  void erase(A const& a);

* Typ point_type umożliwiający dostęp do "punktów" funkcji, udostępniający
  następujące funkcje:
    // zwraca argument funkcji
    A const& arg() const;

    // zwraca wartość funkcji w tym punkcie
    V const& value() const;

  Nie powinno być możliwe bezpośrednie konstruowanie obiektów typu
  point_type, ale zezwalamy na kopiowanie i przypisywanie.

* Typ iterator zachowujący się tak jak bidirectional_iterator
  (http://www.cplusplus.com/reference/iterator/BidirectionalIterator/),
  iterujący po punktach funkcji.
  Dla zmiennej it typu iterator *it powinno być typu point_type const&.

* Funkcje dające dostęp do punktów funkcji:
  // iterator wskazujący na pierwszy punkt
  iterator begin() const;

  // iterator wskazujący za ostatni punkt
  iterator end() const;

  // iterator wskazujacy na punkt o argumencie a lub end(), jeśli takego nie ma
  iterator find(A const& a) const;

  Powyższe trzy funkcje powinny działać w czasie nie gorszym niż O(log n).
  Przejście po wszystkich punktach funkcji (np. w poniższy sposób) powinno
  odbywać się w czasie O(n) i w kolejności rosnących argumentów.

  FunctionMaxima<int, int> F;
  // ...
  for (const auto& p : F) {
    std::cout << p.arg() << " -> " << p.value() << std::endl;
  }

* Typ mx_iterator zachowujący sie znów jak bidirectional_iterator,
  iterujący po lokalnych maksimach funkcji.
  Dla zmiennej it typu iterator *it powinno być typu point_type const&.

* Funkcje dające dostęp do lokalnych maksimów funkcji:
  // iterator wskazujący na pierwsze lokalne maksimum
  mx_iterator mx_begin() const;

  // iterator wskazujący za ostatnie lokalne maksimum
  mx_iterator mx_end() const;

  Jeśli przez k oznaczymy rozmiar zbioru lokalnych maksimów, to powyższe
  funkcje powinny działać w czasie nie gorszym niż O(log k).
  Przejście (nieprzeplatane z modyfikacją funkcji) po wszystkich punktach
  funkcji powinno odbywać się w czasie O(k) i w kolejności malejących wartości.
  W szczególności oznacza to, że w przypadku funkcji F o niepustej dziedzinie
  F.mx_begin()->value() jest największą wartością tej funkcji.

  W przypadku takich samych wartości dwóch maksimów, najpierw powinniśmy
  dotrzeć do punktu o mniejszym argumencie.

* Typ size_type reprezentujący rozmiar dziedziny i funkcja zwracająca ten rozmiar:
  size_type size() const;

Zakładamy, że
* klasy A i V mają konstruktory kopiujące;
* dostępne są:
  bool operator<(A const& x, A const &y);
  bool operator<(V const& x, V const &y);
* w powyższym opisie równość a i b oznacza !(a < b) && !(b < a);
  generalnie, o porządku < zakładamy to samo, co domyślnie
  zakłada np. std::set.

Dodatkowo:
* Wszystkie operacje na funkcji powinny gwarantować silną odporność
  na wyjątki, a tam gdzie jest to możliwe i pożądane, powinny być no-throw.
* Klasa powinna być przezroczysta na wyjątki, czyli powinna przepuszczać
  wszelkie wyjątki zgłaszane przez wywoływane przez nią funkcje i przez
  operacje na jej składowych.
* Uznajemy, że argumenty i wartości funkcji mogą być obiektami niemałych
  rozmiarów. Reprezentacja funkcji powinna zatem utrzymywać jak najmniej
  kopii argumentów i wartości. W szczególności, dobrze byłoby, aby
  funkcja będąca kopią innej funkcji współdzieliła z nią argumenty
  i wartości.
* Wycieki pamięci są zabronione. :)
* Klasa InvalidArg powinna dziedziczyć po std::exception.

=== Przykład użycia ===

```
#include "function_maxima.h"

#include <cassert>
#include <iostream>
#include <vector>

class Secret {
 public:
  int get() const {
    return value;
  }
  bool operator<(const Secret& a) const {
    return value < a.value;
  }
  static Secret create(int v) {
    return Secret(v);
  }
 private:
  Secret(int v) : value(v) {}
  int value;
};

template<typename A, typename V>
struct same {
  bool operator()(const typename FunctionMaxima<A, V>::point_type &p,
                  const std::pair<A, V> &q) {
    return !(p.arg() < q.first) && !(q.first < p.arg()) &&
      !(p.value() < q.second) && !(q.second < p.value());
  }
};

template<typename A, typename V>
bool fun_equal(const FunctionMaxima<A, V> &F,
               const std::initializer_list<std::pair<A, V>> &L) {
  return F.size() == L.size() &&
    std::equal(F.begin(), F.end(), L.begin(), same<A, V>());
}

template<typename A, typename V>
bool fun_mx_equal(const FunctionMaxima<A, V> &F,
                  const std::initializer_list<std::pair<A, V>> &L) {
  return (size_t)std::distance(F.mx_begin(), F.mx_end()) == L.size() &&
    std::equal(F.mx_begin(), F.mx_end(), L.begin(), same<A, V>());
}

int main() {
  FunctionMaxima<int, int> F;
  F.set_value(0, 1);
  assert(fun_equal(F, {{0, 1}}));
  assert(fun_mx_equal(F, {{0, 1}}));

  F.set_value(0, 0);
  assert(fun_equal(F, {{0, 0}}));
  assert(fun_mx_equal(F, {{0, 0}}));

  F.set_value(1, 0);
  F.set_value(2, 0);
  assert(fun_equal(F, {{0, 0}, {1, 0}, {2, 0}}));
  assert(fun_mx_equal(F, {{0, 0}, {1, 0}, {2, 0}}));

  F.set_value(1, 1);
  assert(fun_mx_equal(F, {{1, 1}}));

  F.set_value(2, 2);
  assert(fun_mx_equal(F, {{2, 2}}));
  F.set_value(0, 2);
  F.set_value(1, 3);
  assert(fun_mx_equal(F, {{1, 3}}));

  try {
    std::cout << F.value_at(4) << std::endl;
    assert(false);
  } catch (InvalidArg &e) {
    std::cout << e.what() << std::endl;
  }

  F.erase(1);
  assert(F.find(1) == F.end());
  assert(fun_mx_equal(F, {{0, 2}, {2, 2}}));

  F.set_value(-2, 0);
  F.set_value(-1, -1);
  assert(fun_mx_equal(F, {{0, 2}, {2, 2}, {-2, 0}}));

  std::vector<FunctionMaxima<Secret, Secret>::point_type> v;
  {
    FunctionMaxima<Secret, Secret> temp;
    temp.set_value(Secret::create(1), Secret::create(10));
    temp.set_value(Secret::create(2), Secret::create(20));
    v.push_back(*temp.begin());
    v.push_back(*temp.mx_begin());
  }
  assert(v[0].arg().get() == 1);
  assert(v[0].value().get() == 10);
  assert(v[1].arg().get() == 2);
  assert(v[1].value().get() == 20);

  // to powinno dzialac raczej szybko
  FunctionMaxima<int, int> big;
  const int N = 100000;
  for (int i = 1; i <= N; ++i) {
    big.set_value(i, i);
  }
  int counter = 0;
  for (int i = 1; i <= N; ++i) {
    big.set_value(i, big.value_at(i) + 1);
    for (auto it = big.mx_begin(); it != big.mx_end(); ++it) {
      ++counter;
    }
  }
  assert(counter == 2 * N - 1);
  return 0;
}
```

Powyższy program powinien zakończyć się powodzeniem i wypisać coś w stylu:

InvalidArg

=== Ustalenia techniczne ===

Rozwiązanie powinno być zawarte w pliku function_maxima.h, który należy
umieścić w repozytorium w katalogu

grupaN/zadanie5/ab123456+cd123456

lub

grupaN/zadanie5/ab123456+cd123456+ef123456

gdzie N jest numerem grupy, a ab123456, cd123456, ef123456 są identyfikatorami
członków zespołu umieszczającego to rozwiązanie.
Katalog z rozwiązaniem nie powinien zawierać innych plików, ale może zawierać
podkatalog private, gdzie można umieszczać różne pliki, np. swoje testy.
Pliki umieszczone w tym podkatalogu nie będą oceniane.
