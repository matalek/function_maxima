/* Function_maxima
 * kf346875 & am347171
 * 
 * Wersja na 18.12
 */
 
#ifndef FUNCTION_MAXIMA_H
#define FUNCTION_MAXIMA_H

#include <algorithm>
#include <memory>
#include <iterator>
#include <set>
#include <vector>

// wyjątek rzucany, gdy podana wartość nie należy do dziedziny funkcji
class InvalidArg
    : public std::exception {
    
    public:
        const char* what() const throw() {
            return "InvalidArg";
        }
};

template <typename A, typename V>
class FunctionMaxima {
    public:
        FunctionMaxima() = default;
        
        FunctionMaxima(const FunctionMaxima& other) = default;
        
        // Strong guarantee ze względu na no-throw guarantee swap(multiset)
        FunctionMaxima& operator=(FunctionMaxima other) {
            swap(points, other.points); //no-throw
            swap(mx_points, other.mx_points); //no-throw
            return *this;
        }
        
        // Zwraca wartość w punkcie a, rzuca wyjątek InvalidArg, jeśli a nie
        // należy do dziedziny funkcji.
        // Strong guarentee (ze względu na const).
        V const& value_at(A const& a) const {
            std::shared_ptr<const A> to_find{&a, null_deleter()};
            auto it = points.find(point_type{to_find});
            if (it == points.end())
                throw InvalidArg{};
            else
                return it->value();
        }
        
        // Zmienia funkcję tak, żeby zachodziło f(a) = v. Jeśli a nie 
        // należy do obecnej dziedziny funkcji, jest do niej dodawany.
        // Strong guarentee.
        void set_value(A const& a, V const& v) {
            std::shared_ptr<const A> to_find{&a, null_deleter()};
            std::shared_ptr<const V> v_ptr = std::make_shared<const V>(v);
            auto it = points.find(point_type{to_find});
            
            Insertion<points_set, point_type> points_insert{points};
            Insertion<mx_set, point_type> mx_insert{mx_points};
            Erasion<mx_set> mx_erase{mx_points};
            
            if (it == points.end()) {
                // dodawanie nowego argumentu do dziedziny
                // i przyporządkowanie mu wartości
                std::shared_ptr<const A> a_ptr = std::make_shared<const A>(a);
                
                points_insert.add(point_type{a_ptr, v_ptr});
                
                update_mx(point_type{a_ptr, v_ptr}, mx_insert, mx_erase, points.end()); //może lepiej null
                
            } else if (it->value() < v || v < it->value()) {
                // uaktualnienie wartości dla argumentu w dziedzinie
                Erasion<points_set> points_erase{points};
                
                // usuwanie aktualnego punktu
                auto to_erase = mx_points.find(*it);
                if (to_erase != mx_points.end())
                    mx_erase.add(to_erase); 
                points_erase.add(it);
                
                points_insert.add(point_type{it->a_ptr, v_ptr});
                update_mx(point_type{it->a_ptr, v_ptr}, mx_insert, mx_erase, it);
                
                points_erase.commit();
            }

            points_insert.commit();
            mx_insert.commit();
            mx_erase.commit();
        }
        
        // Usuwa a z dziedziny funkcji. Jeśli a nie należało do dziedziny funkcji,
        // nie dzieje się nic.
        // Strong guarentee.
        void erase(A const& a) {
            std::shared_ptr<const A> to_find(&a, null_deleter());
            auto it = points.find(point_type{to_find});
            
            if (it != points.end()) {

                Insertion<mx_set, point_type> mx_insert{mx_points};
                Erasion<points_set> points_erase{points};
                Erasion<mx_set> mx_erase{mx_points};
                
                // usuwanie aktualnego punktu
                auto to_erase = mx_points.find(*it);
                if (to_erase != mx_points.end())
                    mx_erase.add(to_erase);
                points_erase.add(it);
                
                // uaktualnianie maksimów - nie używam funkcji update_mx,
                // gdyż w tym przypadku jest to istotnie prostsze
                
                auto it_temp = it;
                if (it != points.begin()) {
                    --it_temp;
                    update_single_mx(it_temp, mx_insert, mx_erase, it);
                }
                
                it_temp = it;
                ++it_temp;
                if (it_temp != points.end())
                    update_single_mx(it_temp, mx_insert, mx_erase, it);
                
                points_erase.commit();
                mx_insert.commit();
                mx_erase.commit();  
            }
        }
        
        // Typ umożliwiający dostęp do "punktów" funkcji
        struct point_type {
            public:
            
                // zwraca argument funkcji
                A const& arg() const noexcept {
                    return *a_ptr;
                }

                // zwraca wartość funkcji w tym punkcie
                V const& value() const noexcept {
                    return *v_ptr;
                }
            
            private:
                
                friend class FunctionMaxima;
                
                // konstruktor "realnego" punktu
                point_type(const std::shared_ptr<const A>& a_ptr, const std::shared_ptr<const V>& v_ptr) noexcept 
                    : a_ptr(a_ptr), v_ptr(v_ptr) { }
                
                // konstruktor punktu do wyszukiwania
                point_type(const std::shared_ptr<const A>& a) noexcept 
                    : a_ptr(a) { }
            
                std::shared_ptr<const A> a_ptr;
                std::shared_ptr<const V> v_ptr;
        };
        
        // Typ zachowujący się tak jak bidirectional_iterator 
        // iterujący po punktach funkcji.
        using iterator = typename std::multiset<point_type>::const_iterator;
        
        // iterator wskazujący na pierwszy punkt
        iterator begin() const noexcept {
            return points.begin();
        }
        
        // iterator wskazujący za ostatni punkt
        iterator end() const noexcept {
            return points.end();
        }
        
        // iterator wskazujacy na punkt o argumencie a lub end(),
        // jeśli takego nie ma
        iterator find(A const& a) const {
            std::shared_ptr<const A> to_find(&a, null_deleter());
            return points.find(point_type{to_find});        
        }
        
        // Typ mx_iterator zachowujący sie jak bidirectional_iterator,
        // iterujący po lokalnych maksimach funkcji.
        
        using mx_iterator = typename std::multiset<point_type>::const_iterator;

        // iterator wskazujący na pierwsze lokalne maksimum
        mx_iterator mx_begin() const noexcept {
            return mx_points.begin();
        }
        // iterator wskazujący za ostatnie lokalne maksimum
        mx_iterator mx_end() const noexcept {
            return mx_points.end();
        }
            
        // Typ reprezentujący rozmiar dziedziny 
        using size_type = size_t;
        
        // Funkcja zwracająca rozmiar dziedziny
        size_type size() const noexcept {
            return points.size();
        }
        
    private:
        
        // Nic nierobiący destruktor wykorzystywany przy tworzeniu
        // shared_ptr do porównania.
        struct null_deleter {
            void operator()(void const *) const noexcept { }
        };

        // Porównanie punktów względem argumentów.
        struct point_compare {
            bool operator() (const point_type& lhs, const point_type& rhs) const {
                return lhs.arg() < rhs.arg();
            }
        };
        
        // Porównanie punktów maksimów - wcześniej w porządku
        // leksykograficznym są maksima o większej wartości, a w
        // przypadku równej wartości wcześniej są te, o mniejszym
        // argumencie.
        struct mx_compare {
            bool operator() (const point_type& lhs, const point_type& rhs) const {
                return (equal(lhs.value(), rhs.value())) ? lhs.arg() < rhs.arg() : rhs.value() < lhs.value();
            }
            
            private:
                inline bool equal(const V & lhs, const V & rhs) const {
                    return !((lhs < rhs) || (rhs < lhs));
                }
        };  
    
        // reprezentacja punktów funkcji
        using points_set = std::multiset<point_type, point_compare>;
        points_set points;
        
        // reprezentacja maksimów lokalnych funkcji
        using mx_set = std::multiset<point_type, mx_compare>;
        mx_set mx_points;
        
        // Struktura umożliwiająca bezpieczne wstawiania elementów typu E
        // do skruktury typu T (będącej pewnym multisetem)
        template <typename T, typename E>
        struct Insertion {
            
            Insertion(T& s) : s(s), commited(false) { }
            
            Insertion(Insertion&) = delete;
            Insertion& operator=(const Insertion&) = delete;
            
            // Dodanie elementu
            void add(E v) {
                its.push_back(s.insert(v));
            }
            
            // Odwrócenie zmian w przypadku rzucenia wyjątku
            // No throw guarantee.
            ~Insertion() {
                if(!commited)
                    for(unsigned i = 0; i < its.size(); ++i)
                        s.erase(its[i]); // usuwanie i dostęp do elementów - no throw
            }
            
            // Potwierdzenie zmian.
            void commit() noexcept {
                commited = true;
            }

            private: 
                T& s;
                std::vector<typename T::iterator> its;
                bool commited;
        };
        
        // Struktura umożliwiająca bezpieczne usuwanie iteratorów
        // ze skruktury typu T (będącej pewnym multisetem).
        template <typename T>
        struct Erasion {
            
            Erasion(T& s) : s(s), commited(false) { }
            
            Erasion(Erasion&) = delete;
            Erasion& operator=(const Erasion&) = delete;
            
            // Dodanie elementu z przeznaczeniem do usunięcia.
            // Wskazany iterator nie jest tutaj usuwany.
            void add(typename T::iterator v) {
                its.push_back(v);
            }
            
            // Fizyczne usunięcie dodanych iteratorów.
            void commit() noexcept {
                if(!commited) {
                    commited = true;
                    for(unsigned i = 0; i < its.size(); ++i)
                        s.erase(its[i]); // usuwanie i dostęp do elementów - no throw
                }
            }
            
            private: 
                T& s;
                std::vector<typename T::iterator> its;
                bool commited;
        };

        
        // Sprawdza, czy dany punkt jest lokalnym ekstremum.
        // it - sprawdzany iterator, to_delete - iterator, który jest
        // przeznaczony do usunięcia - trzeba go omijać przy przeglądaniu
        bool is_mx(const typename points_set::iterator& it,
            const typename points_set::iterator& to_delete) const {
            
            auto it_next = it; // tu będzie iterator do następnego punktu
            auto it_prev = it; // tu będzie iterator do wcześniejszego punktu
            ++it_next;
            
            if (it_prev != points.begin()) {
                --it_prev;
                if (it_prev == to_delete) {
                    if (it_prev != points.begin()) 
                        --it_prev;
                    else
                        ++it_prev;
                }
            }
            
            if (it_next == to_delete && it_next != points.end()) ++it_next;

            return ((it == it_prev || !(it->value() < it_prev->value()))
                && (it_next == points.end() || !(it->value() < it_next->value())));
        }
        
        // Uaktualnia pojedynczy punkt jako lokalne maksimum.
        // it - wskaźnik do aktualizowanego punktu,
        // mx_insert, mx_erase - struktury umożliwiające bezpieczne
        // wstawianie i usuwanie, to_delete - iterator, który jest
        // przeznaczony do usunięcia
        void update_single_mx(const typename points_set::iterator& it,
            Insertion<mx_set, point_type>& mx_insert,
            Erasion<mx_set>& mx_erase,
            const typename points_set::iterator& to_delete) {
                
            auto mx_it = mx_points.find(*it);
            if (mx_it == mx_points.end()) {
                if (is_mx(it, to_delete))
                    mx_insert.add(*it);
            } else {
                if (!is_mx(it, to_delete))
                    mx_erase.add(mx_it);
            }
        }
        
        // Uaktualnia lokalne maksima w funkcji, po wstawieniu lub 
        // zmienieniu wartości.
        // to_upadate - wskaźnik do nowo wstawionego punktu,
        // mx_insert, mx_erase - struktury umożliwiające bezpieczne
        // wstawianie i usuwanie, to_delete - iterator, który jest
        // przeznaczony do usunięcia
        void update_mx(const point_type& to_update, 
            Insertion<mx_set, point_type>& mx_insert,
            Erasion<mx_set>& mx_erase,
            const typename points_set::iterator& to_delete) {
            
            auto it = points.lower_bound(to_update);
            if (it == to_delete)
                ++it;
                
            if (is_mx(it, to_delete))
                mx_insert.add(*it);
            
            // aktualizacja punktu wcześniejszego
            if (it != points.begin()) {
                --it;
                if (it == to_delete) {
                    if (it != points.begin()) {
                        --it;
                        update_single_mx(it, mx_insert, mx_erase, to_delete);
                        ++it;
                    }
                } else
                    update_single_mx(it, mx_insert, mx_erase, to_delete);
                ++it;
            }
            
            // aktualizacja punktu następnego
            it++;
            if (it == to_delete && it != points.end())
                it++;
            if (it != points.end()) {
                update_single_mx(it, mx_insert, mx_erase, to_delete);
            }
        }
};

#endif // FUNCTION_MAXIMA_H
